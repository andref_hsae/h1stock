package org.far.hsae.h1stock.request;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Rect;
import android.os.Build;
import android.os.Environment;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBar;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.app.ListActivity;
import android.widget.AutoCompleteTextView;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.far.hsae.h1stock.request.Adapter.RequestAdapter;
import org.far.hsae.h1stock.request.model.Product;
import org.far.hsae.h1stock.request.model.ProductModel;
import org.far.hsae.h1stock.request.model.ProductsDataSource;
import org.far.hsae.h1stock.request.model.Request;
import org.far.hsae.h1stock.request.model.RequestsDataSource;

import java.util.List;


public class MainActivity extends ActionBarActivity
        implements QuantityDialogFragment.OnFragmentInteractionListener
        , RequestDialogFragment.OnFragmentInteractionListener {

    private RequestsDataSource Rdatasource = null;

    private RequestAdapter adapter;

    private HashMap<Integer, Request> SelectedP = null;

    private List<Product> values = null;

    private ProductsDataSource datasource;

    private Rect rect = null;

    private Request parent;

    private final String dpath = "/Exports_H1Stock/";

    private Comparator ComparatorRequest = new Comparator<Request>() {
        @Override
        public int compare(Request request, Request request2) {
            return request.name.compareTo(request2.name);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTheme(R.style.AppTheme);
        setContentView(R.layout.activity_main);

        if (datasource == null) {
            datasource = new ProductsDataSource(this);
            datasource.open();
        }
        values = datasource.getAllProducts();
        datasource.close();

        Log.d("main activi===", "start");
        Rdatasource = new RequestsDataSource(this);
        Request tmp = Rdatasource.getLastRequest();
        if ( tmp != null ) {
            Log.i("hsae.h1stock.mainactivity", "Found a last request " + tmp.name);
            parent = tmp;
        } else {
            parent = new Request();
            parent.code = "18072014";
            parent.name = "Start by andref";
            parent.id = 1;
        }

        ListView listview = (ListView) findViewById(R.id.listView);

        listview.setOnItemClickListener(
                new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                        Request r = (Request)adapterView.getItemAtPosition(i);
                        Toast.makeText(
                                getApplicationContext(), "A abrir " + r.name, Toast.LENGTH_SHORT
                        ).show();
                        QuantityDialogFragment qdf = new QuantityDialogFragment();
                        qdf.setRequestId(Integer.valueOf(r.code));
                        qdf.show(getSupportFragmentManager(), "com.far.hsae.h1stock.qdf");
                    }
                }
        );

        if (Rdatasource != null ) {
            Log.d("hsae.h1stock.listrequests", "in ");

            Rdatasource.open();
            SelectedP = Rdatasource.getAllListRequestsByRequest(parent);
            Rdatasource.close();
            Log.d("hsae.h1stock.listrequests", " total size " + SelectedP.size());

            adapter = new RequestAdapter(this, new ArrayList<Request>(SelectedP.values()));
            adapter.sort(ComparatorRequest);
            listview.setAdapter(adapter);
        }

        Log.d("main activi===", "after list view ");

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        
        // Inflate the menu; this adds items to the action bar if it is present.

        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    protected void onDestroy()
    {
        super.onDestroy();
        if (datasource != null) {
            datasource.close();
        }
    }

    public static int ActivityRequestCode = 1224;

    public void add(View v)
    {
        Intent xI = new Intent(this, ItemRequest.class);
        startActivityForResult(xI, ActivityRequestCode);
    }

    public Context getActivity() {
        return this;
    }

    @Override
    /**
     * change quantity for item in the list
     *
     */
    public void onDialogPositiveClick(QuantityDialogFragment dialog) {
        Log.d("hsae.h1stock.mainactivity", "hit positive ");

        // set qtt
        if (SelectedP.get(dialog.getRequestId()) != null ) {
            if (Rdatasource.updateQuantity(dialog.qtt, dialog.getRequestId())) {
                SelectedP.get(dialog.getRequestId()).quantity = dialog.qtt;
                Toast.makeText(getApplicationContext(), "Alterado", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(getApplicationContext(), "Falha a alterar.", Toast.LENGTH_SHORT).show();
            }

            adapter.notifyDataSetChanged();

        } else {
            Toast.makeText(getApplicationContext(), "Não alterado", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onDialogNegativeClick(QuantityDialogFragment dialog) {
        Log.d("hsae.h1stock.mainactivity", "hit negative");
        Toast.makeText(getApplicationContext(), "eish ", Toast.LENGTH_SHORT).show();
    }

    public void exportCSV(View v) {
        if (SelectedP == null) {
            return;
        }

        Writer writer = null;
        File export = getExportStorageDir();
        try {
            export.createNewFile();
            writer = new BufferedWriter(new OutputStreamWriter(
                    new FileOutputStream(export.getAbsoluteFile()), "utf-8"));

        } catch (IOException e) {
            Log.e("hsae.h1stock.exportCSV", e.getMessage());
            e.printStackTrace();
        }

        try {
            writer.write("ID;Code;Nome;Quantidade\n");
            writer.flush();
            for (final Request r : SelectedP.values()) {
                writer.append(r.toCsv());
            }
        } catch (IOException ex) {
            Log.e("hsae.h1stock.exportCSV", ex.getMessage());
            Toast
                 .makeText(getApplicationContext()
                         , "Falha a escrever para ficheiro. " + ex.getMessage()
                         , Toast.LENGTH_SHORT
                 )
                 .show();
        } finally {

            try {
                writer.flush();
                writer.close();
                if (export.exists()) {
                    Toast.makeText(getApplicationContext(), "Ficheiro guardado."
                            + export.getName(), Toast.LENGTH_SHORT
                    ).show();
                }
            } catch (Exception ex) {}

            return;
        }
    }

    public File getExportStorageDir() {
        if (!isExternalStorageWritable()) {
            Log.e("hsae.h1stock.mainactivity.exportcsv", "External storage not writable");
            Toast
                    .makeText(getApplicationContext()
                            , "Storage não acessivel. Cartão colocado?"
                            , Toast.LENGTH_SHORT
                    )
                    .show();

        }
        // Get the directory for the user's public pictures directory.
        File file = new File(Environment.getExternalStorageDirectory().getPath(),dpath);

        if (!file.exists() && !file.mkdirs()) {
            Log.e("hsae.h1stock.getexportdir", "Directory not created");
        }

        try {
            file = new File(Environment.getExternalStorageDirectory().getPath() + dpath + parent.getFileName());
            file.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
            Log.e("hsae.h1stock.getExportStorage.createNewFile", e.getMessage());
            Toast
                    .makeText(getApplicationContext(),
                            "Falha a criar ficheiro", Toast.LENGTH_SHORT
                    )
            .show();
        }

        return file;
    }

    /* Checks if external storage is available for read and write */
    public boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            return true;
        }
        return false;
    }

    /* Checks if external storage is available to at least read */
    public boolean isExternalStorageReadable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state) ||
                Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
            return true;
        }
        return false;
    }

    public void setNewRequest(View v) {
        RequestDialogFragment rdf = new RequestDialogFragment();
        rdf.show(getSupportFragmentManager(), "hsae.h1stock.requestdialog");
    }

    public void clearAuto(View v) {
        AutoCompleteTextView auto = (AutoCompleteTextView) v.findViewById(R.id.autoCompleteTextView);

        auto.getText().clear();
    }

    @Override
    public void onDialogPositiveClick(RequestDialogFragment dialog) {
        Request r = new Request();
        r.datain = dialog.Datein;
        r.code = dialog.Datein.toUpperCase().replace(" ", "").replace("/","").replace("-", "");
        r.name = dialog.Datein;
        RequestsDataSource rds = new RequestsDataSource(getApplicationContext());

        parent = rds.createParentRequest(r);
        if (parent == null ) {
            Toast.makeText(
                    getApplicationContext(), "Falha a colocar novo pedido: actual fica "
                            + parent.name,Toast.LENGTH_SHORT
            ).show();
            return;
        }
        // clear the current list
        SelectedP.clear();
        adapter.notifyDataSetChanged();
        // novo pedido
        Toast.makeText(
                getApplicationContext(), "Novo Pedido ativo " + parent.name,Toast.LENGTH_SHORT
        ).show();
    }

    @Override
    public void onDialogNegativeClick(RequestDialogFragment dialog) {
        Toast.makeText(
                getApplicationContext(), "Pedido ativo " + parent.name, Toast.LENGTH_SHORT
        ).show();
    }

    public void onActivityResult (int requestCode, int resultCode, Intent data)
    {
        if (requestCode == ActivityRequestCode && resultCode == RESULT_OK && data != null ) {
            String textName = data.getStringExtra("ITEM");
            int Qtt;
            Qtt = data.getIntExtra("QTT", 0);
            itemAdd(textName, Qtt);
        }
    }

    public void itemAdd(String textName, int Qtt) {
        if (SelectedP == null) {
            SelectedP = new HashMap<Integer,Request>();
        }

        int selectedPos = 0;
        for (Product p : values) {
            if (p.toString().contains(textName)) {
                Log.d("Selected ", p.toString());
                Request r = p.toRequest();
                if (textName.toString().compareTo("") != 0 ) {
                    r.quantity = Qtt;
                } else {
                    return;
                }

                Request x = null;
                if (SelectedP.get(Integer.valueOf(r.code)) == null ) {
                    x = Rdatasource.createRequest(r, (int) parent.id);
                } else {
                    Toast.makeText(getApplicationContext(), "Já existe: "
                                    + r.toString(), Toast.LENGTH_LONG
                    ).show();
                    return;
                }

                if (x != null && SelectedP.get(Integer.valueOf(r.code)) == null ) {
                    SelectedP.put(Integer.valueOf(r.code), r);
                    Toast.makeText(getApplicationContext(), "Guardado "
                                    + String.valueOf(x.id) + " / " + x.toString(), Toast.LENGTH_LONG
                    ).show();
                    adapter.notifyDataSetChanged();
                } else {
                    Toast.makeText(getApplicationContext(), "Falha a guardar: "
                        + r.toString(), Toast.LENGTH_LONG
                    ).show();
                }

                break;
            }
        }

        final ListView listview = (ListView) findViewById(R.id.listView);
        RequestAdapter adapter = new RequestAdapter(this, new ArrayList<Request>(SelectedP.values()) );
        adapter.sort(ComparatorRequest);
        listview.setAdapter(adapter);

    }

}
