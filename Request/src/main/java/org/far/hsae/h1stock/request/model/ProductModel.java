package org.far.hsae.h1stock.request.model;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;

import java.io.File;

import static android.database.sqlite.SQLiteDatabase.*;

/**
 * Created by andref on 25/06/14.
 */
public class ProductModel {

    public static final String TABLE = "product";
    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_CODE = "code";
    public static final String COLUMN_NAME = "name";

    private static final String DATABASE_NAME = "products.db";
    private static final int DATABASE_VERSION = 1;
    public SQLiteDatabase db;

    // Database creation sql statement
    private static final String DATABASE_CREATE = "create table "
            + TABLE + "(" + COLUMN_ID
            + " integer primary key autoincrement,"  + COLUMN_CODE + " text not null , "
            + COLUMN_NAME
            + " text not null);";


    public ProductModel(Context context) {
        //super(context, DATABASE_NAME, null, DATABASE_VERSION);
        File dbfile = new File(Environment.getExternalStorageDirectory().getPath() + "/" + DATABASE_NAME);
        Log.i("app.hsae.h1stock", "path to file=" + Environment.getExternalStorageDirectory().getPath() + "/" + DATABASE_NAME);
        if (!dbfile.exists()) {
            Log.e("error.hsae.h1stock", "shit happens");
        }
        if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
            if(db == null) {
                db = openOrCreateDatabase(dbfile, null);
            }
        } else {
            db = null;
            Log.e("error.hsae.h1stock", "NO DB ACCESS FOUND");
        }
        //super(context, , null, DATABASE_VERSION);
        //context.openOrCreateDatabase("/sdcard/" + DATABASE_NAME, SQLiteDatabase.CursorFactory );
        Log.d("Product Model===", "construct");
    }

    public void onCreate(SQLiteDatabase database) {
        //database.execSQL(DATABASE_CREATE);

    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        /*Log.w(MySQLiteHelper.class.getName(),
                "Upgrading database from version " + oldVersion + " to "
                        + newVersion + ", which will destroy all old data");
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_COMMENTS);
        onCreate(db);
        */
    }


}
