package org.far.hsae.h1stock.request;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.far.hsae.h1stock.request.Adapter.RequestAdapter;
import org.far.hsae.h1stock.request.model.Product;
import org.far.hsae.h1stock.request.model.ProductsDataSource;
import org.far.hsae.h1stock.request.model.Request;

import java.util.ArrayList;
import java.util.List;


public class ItemRequest extends ActionBarActivity {

    private List<Product> values = null;
    private ProductsDataSource datasource = null ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item_request);

        if (datasource == null) {
            datasource = new ProductsDataSource(this);
            datasource.open();
        }
        values = datasource.getAllProducts();
        datasource.close();

        ArrayAdapter<Product> adapter1;
        adapter1 = new ArrayAdapter<Product>(
                getApplicationContext()
                , R.layout.support_simple_spinner_dropdown_item
                , values
        );

        AutoCompleteTextView textView = (AutoCompleteTextView)
                findViewById(R.id.autoCompleteTextView);
        textView.setThreshold(3);
        textView.setAdapter(adapter1);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.item_request, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void add(View v)
    {
        AutoCompleteTextView textView = (AutoCompleteTextView)
                findViewById(R.id.autoCompleteTextView);
        EditText qtt = (EditText) findViewById(R.id.requestQuantity);
        Intent output = new Intent();
        output.putExtra("ITEM", textView.getText().toString());
        output.putExtra("QTT", Integer.valueOf(qtt.getText().toString()));
        setResult(RESULT_OK, output);
        finish();
    }

    public void noAdd(View v) {
        setResult(RESULT_CANCELED, null);
        finish();
    }
}
