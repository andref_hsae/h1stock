package org.far.hsae.h1stock.request.model;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class ProductsDataSource {

    // Database fields
    private SQLiteDatabase database;
    private ProductModel dbHelper;
    private String[] allColumns = { ProductModel.COLUMN_ID,
            ProductModel.COLUMN_CODE, ProductModel.COLUMN_NAME };

    public ProductsDataSource(Context context) {
        ProductModel ProductM = new ProductModel(context);
        dbHelper = ProductM;
    }

    public void open() throws SQLException {
        database = dbHelper.db;
    }

    public void close() {
        dbHelper.db.close();
    }

    public Product createProduct(String Product) {
        ContentValues values = new ContentValues();
        values.put(ProductModel.COLUMN_NAME, Product);
        long insertId = database.insert(ProductModel.TABLE, null,
                values);
        Cursor cursor = database.query(ProductModel.TABLE,
                allColumns, ProductModel.COLUMN_ID + " = " + insertId, null,
                null, null, null);
        cursor.moveToFirst();
        Product newComment = cursorToProduct(cursor);

        cursor.close();
        return newComment;
    }

    public void deleteComment(Product product) {
        long id = product.id;
        System.out.println("Comment deleted with id: " + id);
        database.delete(ProductModel.TABLE, ProductModel.COLUMN_ID
                + " = " + id, null);
    }

    public List<Product> getAllProducts() {
        List<Product> products = new ArrayList<Product>();
        Cursor cursor = null;
        try {
            cursor = database.query(ProductModel.TABLE,
                    allColumns, null, null, null, null, null);

            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                Product comment = cursorToProduct(cursor);
                products.add(comment);
                cursor.moveToNext();
            }
        } catch (Exception e ) {
            Log.e("hsae.h1stock", "found error");
        } finally {
            // make sure to close the cursor
            if (cursor != null) {
                cursor.close();
            }
        }

        return products;
    }

    private Product cursorToProduct(Cursor cursor) {
        Product comment = new Product();
        comment.id = cursor.getLong(0);
        comment.code = cursor.getString(1);
        comment.name = cursor.getString(2);
        return comment;
    }
} 