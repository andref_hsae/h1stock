package org.far.hsae.h1stock.request.model;

/**
 * Created by andref on 25/06/14.
 */
public class Request {
    public long id;
    public String code;
    public String name;

    public Integer quantity = 0; // quantity

    public String datain;

    @Override
    public String toString()
    {
        if (quantity > 0 ) {
            return name + " " + quantity.toString();
        }
        return name;
    }

    public String toCsv()
    {
        return String.valueOf(id) + ";" + code + ";" + name + ";" + quantity + "\n";
    }

    public String getFileName() {
        return code.toString().replace(" ", "") + ".csv";
    }
}
