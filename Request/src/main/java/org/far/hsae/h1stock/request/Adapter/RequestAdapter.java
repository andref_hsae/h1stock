package org.far.hsae.h1stock.request.Adapter;

import android.app.AlertDialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.far.hsae.h1stock.request.MainActivity;
import org.far.hsae.h1stock.request.QuantityDialogFragment;
import org.far.hsae.h1stock.request.R;
import org.far.hsae.h1stock.request.model.Request;

import java.util.ArrayList;

/**
 * Created by andref on 01/07/14.
 */
public class RequestAdapter extends ArrayAdapter<Request> {

    public RequestAdapter(Context context, ArrayList<Request> requests) {
        super(context, R.layout.activity_listview, requests);
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        Request Request = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext())
                    .inflate(R.layout.activity_listview, parent, false);
        }

        // Lookup view for data population
        TextView label = (TextView) convertView.findViewById(R.id.label);
        TextView qtt = (TextView) convertView.findViewById(R.id.labelQt);

        // Populate the data into the template view using the data object
        label.setText(Request.name);

        if (Request.quantity != null) {
            qtt.setText( String.valueOf(Request.quantity));
        }

        // Return the completed view to render on screen
        return convertView;
    }

}
