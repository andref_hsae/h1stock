package org.far.hsae.h1stock.request.model;

/**
 * Created by andref on 25/06/14.
 */
public class Product {
    public long id;
    public String code;
    public String name;

    public Integer quantity = 0; // quantity

    @Override
    public String toString()
    {
        if (quantity > 0 ) {
            return name + " " + quantity.toString();
        }
        return name;
    }

    public Request toRequest() {
        Request r = new Request();
        r.name = name;
        r.code = code;

        return r;
    }
}
