package org.far.hsae.h1stock.request.model;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;

import java.io.File;

import static android.database.sqlite.SQLiteDatabase.openOrCreateDatabase;

/**
 * Created by andref on 25/06/14.
 */
public class RequestModel {

    private final Context context;
    public SQLiteDatabase db = null;

    //Environment.getDataDirectory() + APP_NAME + FILENAME
    /**
     * table request keeps the list of the request name time and code
     */
    public static final String TABLE_REQUEST = "request";
    /**
     * request list keeps the request of products
     */
    public static final String TABLE_REQUEST_LIST = "requestlist";
    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_DATE = "datain";
    public static final String COLUMN_CODE = "code";
    public static final String COLUMN_NAME = "name";
    public static final String COLUMN_QUANTITY = "quantity";
    public static final String COLUMN_REQUEST_ID = "request_id";

    private static final String DATABASE_NAME = "requests.db";
    private static final int DATABASE_VERSION = 1;

    // Database creation sql statement
    private static final String DATABASE_CREATE = "create table IF NOT EXISTS "
            + TABLE_REQUEST + "(" + COLUMN_ID
            + " integer primary key autoincrement,"  + COLUMN_CODE + " text not null , "
            + COLUMN_NAME
            + " text not null "
            + " , " + COLUMN_DATE + "  datetime default current_timestamp "
            + ");";

    private static final String DATABASE_CREATE_2 = "create table IF NOT EXISTS "
            + TABLE_REQUEST_LIST + "(" + COLUMN_ID
            + " integer primary key autoincrement,"  + COLUMN_CODE + " text not null , "
            + COLUMN_NAME + " text not null "
            + " , " + COLUMN_QUANTITY + " int "
            + " , " + COLUMN_DATE + "  datetime default current_timestamp "
            + " , " + COLUMN_REQUEST_ID
            + " );";

    public RequestModel(Context context) {
        this.context = context;
        open();
    }

    public void open() {
        File dbfile = new File(Environment.getExternalStorageDirectory().getPath() + "/" + DATABASE_NAME);
        Log.i("app.hsae.h1stock", "path to file=" + Environment.getExternalStorageDirectory().getPath() + "/" + DATABASE_NAME);
        if (!dbfile.exists()) {
            Log.e("error.hsae.h1stock", "shit happens file not writable");
            Toast t = Toast.makeText(this.context, "Ficheiro não existe ou não é possível escrever para o mesmo.", 60);
            t.show();
        }
        if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
            if(db == null || !db.isOpen()) {
                db = openOrCreateDatabase(dbfile, null);
                this.onCreate(db);
            }
        } else {
            db = null;
            Log.e("error.hsae.h1stock", "NO DB ACCESS FOUND MEDIA NOT MOUNTED");
        }
    }

    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        //db.execSQL("DROP TABLE " + TABLE_REQUEST_LIST + ";");
        db.execSQL(DATABASE_CREATE);
        db.execSQL(DATABASE_CREATE_2);
    }

    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i2) {
        //db.execSQL(DATABASE_CREATE);
    }
}
