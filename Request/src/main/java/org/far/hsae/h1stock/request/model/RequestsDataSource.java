package org.far.hsae.h1stock.request.model;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static org.far.hsae.h1stock.request.model.RequestModel.COLUMN_ID;
import static org.far.hsae.h1stock.request.model.RequestModel.COLUMN_NAME;
import static org.far.hsae.h1stock.request.model.RequestModel.COLUMN_QUANTITY;
import static org.far.hsae.h1stock.request.model.RequestModel.TABLE_REQUEST_LIST;

public class RequestsDataSource {

    // Database fields
    private SQLiteDatabase database;
    private RequestModel dbHelper;
    private String[] allColumnsList = { COLUMN_ID,
            RequestModel.COLUMN_CODE, RequestModel.COLUMN_NAME,
            COLUMN_QUANTITY };

    private String[] allColumns = { COLUMN_ID,
            RequestModel.COLUMN_CODE, RequestModel.COLUMN_NAME};
    private String[] allColumnsParent = { COLUMN_ID,
            RequestModel.COLUMN_CODE, RequestModel.COLUMN_NAME,
            RequestModel.COLUMN_DATE};

    public RequestsDataSource(Context context) {
        RequestModel ProductM = new RequestModel(context);
        dbHelper = ProductM;
    }

    public void open() throws SQLException {
        if (dbHelper.db.isOpen()) {
            Log.i("hsae.h1stock", "database is open currently");
        }
        if (dbHelper.db == null || !dbHelper.db.isOpen()) {
            Log.i("hsae.h1stock", "database is being open");
            dbHelper.open();
        }
        database = dbHelper.db;
    }

    public void close() {
        dbHelper.db.close();
    }

    public Request createRequest(Request request, int request_id) {
        if (database == null) {
            open();
        }

        ContentValues values = new ContentValues();
        values.put(RequestModel.COLUMN_CODE, request.code);
        values.put(RequestModel.COLUMN_NAME, request.name);
        values.put(COLUMN_QUANTITY, request.quantity);
        values.put(RequestModel.COLUMN_REQUEST_ID, request_id );

        if (!database.isOpen()) {
            Log.e("hsae.h1stock", "failed DB is not open... trying to open");
            open();
            if (!database.isOpen()) {
                Log.e("hsae.h1stock", "failed DB is not open... no change");
            }
        }

        if (database.isReadOnly()) {
            Log.e("hsae.h1stock", "failed DB is read only.");
        }

        long insertId = database.insert(TABLE_REQUEST_LIST, null,
                values);

        Log.d("hsae.h1stock.createRequest", String.valueOf(insertId) );

        Cursor cursor = null;
        Request newComment = null;
        try {
             cursor = database.query(TABLE_REQUEST_LIST,
                    allColumnsList, COLUMN_ID + " = " + insertId, null,
                    null, null, null);

             cursor.moveToFirst();
            if (cursor.getCount() > 0) {
                newComment = cursorToRequest(cursor);
            }
        } catch (Exception e ) {
            Log.e("hsae.h1stock.createRequest", "Falha a tentar obter registo da bd " + e.getMessage());
        }
        finally{
            cursor.close();
        }
        return newComment;
    }

    public Request createParentRequest(Request request) {
        if (database == null) {
            open();
        }

        ContentValues values = new ContentValues();
        values.put(RequestModel.COLUMN_CODE, request.code);
        values.put(RequestModel.COLUMN_NAME, request.name);
        values.put(RequestModel.COLUMN_DATE, request.datain);

        if (!database.isOpen()) {
            Log.e("hsae.h1stock", "failed DB is not open... trying to open");
            open();
            if (!database.isOpen()) {
                Log.e("hsae.h1stock", "failed DB is not open... no change");
            }
        }

        if (database.isReadOnly()) {
            Log.e("hsae.h1stock", "failed DB is read only.");
        }

        long insertId = database.insert(RequestModel.TABLE_REQUEST, null,
                values);

        Log.d("hsae.h1stock.createRequest", String.valueOf(insertId) );

        Cursor cursor = null;
        Request newComment = null;
        try {
            cursor = database.query(RequestModel.TABLE_REQUEST,
                    allColumnsParent, COLUMN_ID + " = " + insertId, null,
                    null, null, null);

            cursor.moveToFirst();
            if (cursor.getCount() > 0) {
                newComment = cursorToRequest(cursor);
            }
        } catch (Exception e ) {
            Log.e("hsae.h1stock.createRequest", "Falha a tentar obter registo da bd " + e.getMessage());
        }
        finally{
            if (cursor != null) {
                cursor.close();
            }
        }
        return newComment;
    }

    public void deleteRequest(Request request) {
        long id = request.id;
        System.out.println("request deleted with id: " + id);
        database.delete(TABLE_REQUEST_LIST, COLUMN_ID
                + " = " + id, null);
    }

    public List<Request> getAllRequests() {
        if (database == null) {
            open();
        }
        List<Request> requests = new ArrayList<Request>();
        Cursor cursor = null;
        try {
            cursor = database.query(RequestModel.TABLE_REQUEST,
                    allColumns, null, null, null, null, null);

            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                Request comment = cursorToRequest(cursor);
                requests.add(comment);
                cursor.moveToNext();
            }
        } catch (Exception e ) {
            Log.e("hsae.h1stock", "found error " +e.getMessage() );
        } finally {
            // make sure to close the cursor
            if (cursor != null) {
                cursor.close();
            }
        }

        return requests;
    }

    public Request getLastRequest() {
        if (database == null) {
            open();
        }
        Cursor cursor = null;
        try {
            cursor = database.query(RequestModel.TABLE_REQUEST,
                    allColumnsParent, null, null, null, null,
                    COLUMN_ID + " DESC", "1");

            cursor.moveToFirst();
            Request comment = cursorToParent(cursor);
            return comment;

        } catch (Exception e ) {
            Log.e("hsae.h1stock.request.getlast", "found error " + e.getMessage());
        } finally {
            // make sure to close the cursor
            if (cursor != null) {
                cursor.close();
            }
        }

        return null;
    }

    public List<Request> getAllListRequests() {
        List<Request> requests = new ArrayList<Request>();
        Cursor cursor = null;
        try {
            cursor = database.query(TABLE_REQUEST_LIST,
                    allColumnsList, null, null, null, null, null);

            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                Request comment = cursorToRequest(cursor);
                requests.add(comment);
                cursor.moveToNext();
            }
        } catch (Exception e ) {
            Log.e("hsae.h1stock", "found error");
        } finally {
            // make sure to close the cursor
            if (cursor != null) {
                cursor.close();
            }
        }

        return requests;
    }

    public HashMap<Integer, Request> getAllListRequestsByRequest(Request r) {
        if (database == null) {
            open();
        }
        HashMap<Integer, Request> requests = new HashMap<Integer, Request>();
        Cursor cursor = null;
        try {
            cursor = database.query(TABLE_REQUEST_LIST,
                    allColumnsList, RequestModel.COLUMN_REQUEST_ID + " = " + r.id
                    , null, null, null, null
            );
            Log.d("hsae.h1stock.requests.getbyid", r.id + " cursor count "
                    + String.valueOf(cursor.getCount()) + " + " + cursor.toString() );

            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                Request comment = cursorToRequest(cursor);
                requests.put(Integer.valueOf(comment.code), comment);
                Log.d("hsae.h1stock.requests.getbyid.iter", comment.name + " " + comment.code + "  ");
                cursor.moveToNext();
            }
        } catch (Exception e ) {
            Log.e("hsae.h1stock.requests.getbyid", "found error " + e.getMessage());
        } finally {
            // make sure to close the cursor
            if (cursor != null) {
                cursor.close();
            }
        }

        return requests;
    }

    private Request cursorToRequest(Cursor cursor) {
        Request comment = new Request();
        comment.id = cursor.getLong(0);
        comment.code = cursor.getString(1);
        comment.name = cursor.getString(2);
        comment.quantity = cursor.getInt(3);
        return comment;
    }

    private Request cursorToParent(Cursor cursor) {
        Request comment = new Request();
        comment.id = cursor.getLong(0);
        comment.code = cursor.getString(1);
        comment.name = cursor.getString(2);
        comment.datain = cursor.getString(3);
        return comment;
    }

    public boolean updateQuantity(int qtt, int requestId) {
        ContentValues cttv = new ContentValues(1);
        cttv.put(COLUMN_QUANTITY, qtt);
        if (database == null || !database.isOpen()) {
            open();
        }

        int update = database.update(
                TABLE_REQUEST_LIST
                , cttv
                ,String.format("%s = ?", RequestModel.COLUMN_ID)
                ,new String[]{String.valueOf(requestId)}
        );
        if (update == 1 ) {
            return true;
        } else {
            return false;
        }
    }
}