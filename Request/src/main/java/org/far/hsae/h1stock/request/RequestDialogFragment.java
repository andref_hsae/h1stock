package org.far.hsae.h1stock.request;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;

import java.util.Date;


/**
 * A simple {@link android.support.v4.app.Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link org.far.hsae.h1stock.request.RequestDialogFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link org.far.hsae.h1stock.request.RequestDialogFragment#newInstance} factory method to
 * create an instance of this fragment.
 *
 */
public class RequestDialogFragment extends DialogFragment {

    private OnFragmentInteractionListener mListener;
    public String Datein;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment DialogFragment.
     */
    public static RequestDialogFragment newInstance() {
        RequestDialogFragment fragment = new RequestDialogFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = getActivity();
    }

    public void onButtonPressed(Uri uri) {
        Log.d("hsae1.h1stock.RequestDialogFragment.onbuttonpressed", "pressed");
        if (mListener != null) {
            Log.d("hsae1.h1stock.RequestDialogFragment.onbuttonpressed", "not null. go inside");
            mListener.onDialogNegativeClick(this);
            mListener.onDialogPositiveClick(this);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            Log.d("hsae1.h1stock.RequestDialogFragment.onAttach", "attaching");
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    private int requestId = 0;

    public void setRequestId(int requestId) {
        this.requestId = requestId;
    }

    public int getRequestId() {
        return requestId;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        public void onDialogPositiveClick(RequestDialogFragment dialog);
        public void onDialogNegativeClick(RequestDialogFragment dialog);
    }

    private View view;

    private Context context;

    private Context getContext()
    {
        return context;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        // Add the buttons
        builder.setTitle("Novo pedido");
        LayoutInflater inflater = (LayoutInflater) getContext()
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        builder.setView(inflater.inflate(R.layout.dialog_request_new, null));

        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // Send the positive button event back to the host activity
                Log.d("hsae.h1stock.builder.setpositive", "hitting positive " + id);
                Dialog f = (Dialog) dialog;
                DatePicker txtdatein = (DatePicker) f.findViewById(R.id.dateIn);
                int month = txtdatein.getMonth();
                int day = txtdatein.getDayOfMonth();
                int year = txtdatein.getYear();
                Datein = String.valueOf(year) + "-" + String.valueOf(month) + "-" + String.valueOf(day);

                mListener.onDialogPositiveClick(RequestDialogFragment.this);

                Log.d("hsae.h1stock.requestdialog.setpositive",
                        mListener == null ? " not listening " : " listening " + mListener.toString()
                );
            }
        });
        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // User cancelled the dialog
                mListener.onDialogNegativeClick(RequestDialogFragment.this);
            }
        });
        // Set other dialog properties

        // Create the AlertDialog
        AlertDialog dialog = builder.create();
        dialog.show();

        return dialog;
    }
}
